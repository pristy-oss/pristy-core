# Pristy Core

Module ACS contenant de nouveaux :

* modèles de données
* policies
* webservices

Ces éléments seront utilisés dans les interfaces Pristy Vue.

# Test

Pour tester les développer, vous pouvez utiliser la stack docker-compose définie dans le projet.

1. Compile

```
mvn clean package resources:resources
```

2. Démmarrage de la stack

```
docker-compose -f target/docker-compose/docker-compose.yml up --build -d
```

3. Accès

[ACS](http://localhost:8080/alfresco)
[Share](http://localhost:8080/share)
[Api Explorer](http://localhost:8080/api-explorer)

## Versions

| Version | Commits                                                                                |
|---------|:---------------------------------------------------------------------------------------|
| 1.0.0   | Première version publique                                                              |
