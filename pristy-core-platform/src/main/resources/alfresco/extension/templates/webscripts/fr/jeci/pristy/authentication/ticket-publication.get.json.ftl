{
  "ticket": "<#if ticket??>${ticket}</#if>",
  "login": "<#if login??>${login}</#if>",
  "status": {
    "code" : ${status.code?html},
    "message": "${status.message?json_string}"
  }
}