/**
* Copyright 2021 - Jeci SARL - https://jeci.fr
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public License
* along with this program.  If not, see http://www.gnu.org/licenses/.
*/
package fr.jeci.pristy.policy;

import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.repo.node.NodeServicePolicies.OnCreateNodePolicy;
import org.alfresco.repo.policy.Behaviour;
import org.alfresco.repo.policy.Behaviour.NotificationFrequency;
import org.alfresco.repo.policy.JavaBehaviour;
import org.alfresco.repo.policy.PolicyComponent;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.log4j.Logger;

import fr.jeci.pristy.model.ActeModel;
import fr.jeci.pristy.service.ActeService;

/**
 * @author Jeci - Cindy PIASSALE
 *
 */
public class FolderSeancePolicy implements OnCreateNodePolicy {
	private static Logger logger = Logger.getLogger(FolderSeancePolicy.class);

	private PolicyComponent policyComponent;
	private ActeService acteService;

	public void init() {
		Behaviour onCreateFolderSeance = new JavaBehaviour(this, "onCreateNode",
				NotificationFrequency.TRANSACTION_COMMIT);
		this.policyComponent.bindClassBehaviour(OnCreateNodePolicy.QNAME, ActeModel.TYPE_DOSSIER_SEANCE,
				onCreateFolderSeance);
	}

	@Override
	public void onCreateNode(ChildAssociationRef childAssocRef) {
		if (logger.isDebugEnabled()) {
			logger.debug("START FolderSeancePolicy - onCreateNode");
		}
		// Dossier seance
		NodeRef folder = childAssocRef.getChildRef();

		// Test si on est dans un site acte
		if (this.acteService.isInWorkspaceActe(folder)) {
			// Classement du dossier seance
			this.acteService.moveFolderSeance(folder);
		} else {
			logger.error("Création d'un dossier de type 'séance' non autorisé");
			throw new AlfrescoRuntimeException("Création d'un dossier de type 'séance' non autorisé");
		}

		if (logger.isDebugEnabled()) {
			logger.debug("END FolderSeancePolicy - onCreateNode");
		}
	}
	// -------------------------------------------------------------------------- Setters Methods

	/**
	 * @param policyComponent the policyComponent to set
	 */
	public void setPolicyComponent(PolicyComponent policyComponent) {
		this.policyComponent = policyComponent;
	}

	/**
	 * @param acteService the acteService to set
	 */
	public void setActeService(ActeService acteService) {
		this.acteService = acteService;
	}
}
