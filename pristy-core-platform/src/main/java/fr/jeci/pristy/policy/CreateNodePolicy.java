/**
* Copyright 2021 - Jeci SARL - https://jeci.fr
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public License
* along with this program.  If not, see http://www.gnu.org/licenses/.
*/
package fr.jeci.pristy.policy;

import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.node.NodeServicePolicies.OnCreateNodePolicy;
import org.alfresco.repo.policy.Behaviour;
import org.alfresco.repo.policy.Behaviour.NotificationFrequency;
import org.alfresco.repo.policy.JavaBehaviour;
import org.alfresco.repo.policy.PolicyComponent;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;

import fr.jeci.pristy.service.PristyService;

/**
 * @author Jeci - Cindy PIASSALE
 *
 */
public class CreateNodePolicy implements OnCreateNodePolicy {
	private static Logger logger = Logger.getLogger(CreateNodePolicy.class);

	private PolicyComponent policyComponent;
	private NodeService nodeService;
	private NamespaceService namespaceService;
	private PristyService pristyService;

	public void init() {
		Behaviour onCreateNode = new JavaBehaviour(this, "onCreateNode", NotificationFrequency.TRANSACTION_COMMIT);
		this.policyComponent.bindClassBehaviour(OnCreateNodePolicy.QNAME, ContentModel.TYPE_FOLDER, onCreateNode);
		this.policyComponent.bindClassBehaviour(OnCreateNodePolicy.QNAME, ContentModel.TYPE_CONTENT, onCreateNode);
	}

	@Override
	public void onCreateNode(ChildAssociationRef childAssocRef) {
		if (logger.isDebugEnabled()) {
			logger.debug("START CreateNodePolicy - onCreateNode");
		}

		if (!this.pristyService.canCreateNode(childAssocRef)) {
			QName type = this.nodeService.getType(childAssocRef.getChildRef());
			throw new AlfrescoRuntimeException(
					"La création du noeud du type '" + type.toPrefixString(this.namespaceService) + "' n'est pas authorisé");
		}

		if (logger.isDebugEnabled()) {
			logger.debug("END CreateNodePolicy - onCreateNode");
		}

	}

	// -------------------------------------------------------------------------- Setters Methods

	/**
	 * @param policyComponent the policyComponent to set
	 */
	public void setPolicyComponent(PolicyComponent policyComponent) {
		this.policyComponent = policyComponent;
	}

	/**
	 * @param pristyService the pristyService to set
	 */
	public void setPristyService(PristyService pristyService) {
		this.pristyService = pristyService;
	}

	/**
	 * @param nodeService the nodeService to set
	 */
	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}

	/**
	 * @param namespaceService the namespaceService to set
	 */
	public void setNamespaceService(NamespaceService namespaceService) {
		this.namespaceService = namespaceService;
	}

}
