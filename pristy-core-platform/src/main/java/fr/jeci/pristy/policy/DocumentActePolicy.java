/**
* Copyright 2021 - Jeci SARL - https://jeci.fr
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public License
* along with this program.  If not, see http://www.gnu.org/licenses/.
*/
package fr.jeci.pristy.policy;

import java.io.Serializable;
import java.util.Map;

import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.repo.node.NodeServicePolicies.OnCreateNodePolicy;
import org.alfresco.repo.node.NodeServicePolicies.OnUpdatePropertiesPolicy;
import org.alfresco.repo.policy.Behaviour;
import org.alfresco.repo.policy.Behaviour.NotificationFrequency;
import org.alfresco.repo.policy.JavaBehaviour;
import org.alfresco.repo.policy.PolicyComponent;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.security.PermissionService;
import org.alfresco.service.cmr.site.SiteService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import fr.jeci.pristy.model.ActeModel;
import fr.jeci.pristy.model.PristyModel;
import fr.jeci.pristy.service.ActeService;

/**
 * @author Jeci - Cindy PIASSALE
 *
 */
public class DocumentActePolicy implements OnCreateNodePolicy, OnUpdatePropertiesPolicy {
	private static Logger logger = Logger.getLogger(DocumentActePolicy.class);

	private PolicyComponent policyComponent;
	private NodeService nodeService;
	private ActeService acteService;
	private PermissionService permissionService;
	private SiteService siteService;

	public void init() {
		// Création d'un documentActe
		Behaviour onCreateDocumentActe = new JavaBehaviour(this, "onCreateNode",
				NotificationFrequency.TRANSACTION_COMMIT);
		this.policyComponent.bindClassBehaviour(OnCreateNodePolicy.QNAME, ActeModel.TYPE_DOCUMENT_ACTE,
				onCreateDocumentActe);

		// Mise à jour de la propriété am:typeActe
		Behaviour onPropertyTypeActe = new JavaBehaviour(this, "onUpdateProperties",
				NotificationFrequency.TRANSACTION_COMMIT);
		this.policyComponent.bindClassBehaviour(OnUpdatePropertiesPolicy.QNAME, ActeModel.TYPE_DOCUMENT_ACTE,
				onPropertyTypeActe);
	}

	@Override
	public void onCreateNode(ChildAssociationRef childAssocRef) {
		if (logger.isDebugEnabled()) {
			logger.debug("START DocumentActePolicy - onCreateNode");
		}

		NodeRef document = childAssocRef.getChildRef();
		NodeRef parent = childAssocRef.getParentRef();

		// Test si la création d'un document am:documentActe est authorisé
		// - Site de type actes
		// - Parent de type am:dossierSeance
		if (this.nodeService.exists(document) && this.acteService.isInWorkspaceActe(document)
				&& ActeModel.TYPE_DOCUMENT_ACTE.equals(this.nodeService.getType(document))
				&& ActeModel.TYPE_DOSSIER_SEANCE.equals(this.nodeService.getType(parent))) {
			// Recopier la date de seance du dossier
			this.nodeService.setProperty(document, ActeModel.PROP_DATE_SEANCE,
					this.nodeService.getProperty(parent, ActeModel.PROP_DATE_SEANCE));

			// Ajouter role lecteur pour l'utilisateur de publication
			String typeActe = (String) this.nodeService.getProperty(document, ActeModel.PROP_TYPE_ACTE);
			if (!ActeModel.TYPE_ACTE_AI.equals(typeActe)) {
				String userName = (String) this.nodeService.getProperty(this.siteService.getSite(document).getNodeRef(),
						PristyModel.PROP_PUB_USERNAME);
				this.permissionService.setPermission(document, userName, PermissionService.CONSUMER, true);
			}
		} else {
			logger.error("Création d'un document de type acte non autorisé");
			throw new AlfrescoRuntimeException("Création d'un document de type acte non autorisé");
		}

		if (logger.isDebugEnabled()) {
			logger.debug("END DocumentActePolicy - onCreateNode");
		}
	}

	@Override
	public void onUpdateProperties(NodeRef document, Map<QName, Serializable> before, Map<QName, Serializable> after) {
		if (logger.isDebugEnabled()) {
			logger.debug("START DocumentActePolicy - onUpdateProperties");
		}

		// Check le changement de la propriété
		String beforeType = (String) before.get(ActeModel.PROP_TYPE_ACTE);
		String afterType = (String) after.get(ActeModel.PROP_TYPE_ACTE);

		String userName = (String) this.nodeService.getProperty(this.siteService.getSite(document).getNodeRef(),
				PristyModel.PROP_PUB_USERNAME);

		// Suppression du role lecteur si le document acte devient de type AI
		if (StringUtils.isNotBlank(afterType) && ActeModel.TYPE_ACTE_AI.equals(afterType)) {
			this.permissionService.setPermission(document, userName, PermissionService.CONSUMER, false);
		}

		// Ajout du role lecteur si le document acte n'est plus de type AI
		if (StringUtils.isNotBlank(beforeType) && ActeModel.TYPE_ACTE_AI.equals(beforeType)
				&& !beforeType.equals(afterType)) {
			this.permissionService.setPermission(document, userName, PermissionService.CONSUMER, true);
		}

		if (logger.isDebugEnabled()) {
			logger.debug("END DocumentActePolicy - onUpdateProperties");
		}
	}

	// -------------------------------------------------------------------------- Setters Methods

	/**
	 * @param policyComponent the policyComponent to set
	 */
	public void setPolicyComponent(PolicyComponent policyComponent) {
		this.policyComponent = policyComponent;
	}

	/**
	 * @param nodeService the nodeService to set
	 */
	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}

	/**
	 * @param acteService the acteService to set
	 */
	public void setActeService(ActeService acteService) {
		this.acteService = acteService;
	}

	/**
	 * @param permissionService the permissionService to set
	 */
	public void setPermissionService(PermissionService permissionService) {
		this.permissionService = permissionService;
	}

	/**
	 * @param siteService the siteService to set
	 */
	public void setSiteService(SiteService siteService) {
		this.siteService = siteService;
	}

}
