/**
* Copyright 2021 - Jeci SARL - https://jeci.fr
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public License
* along with this program.  If not, see http://www.gnu.org/licenses/.
*/
package fr.jeci.pristy.policy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.node.NodeServicePolicies.OnAddAspectPolicy;
import org.alfresco.repo.node.NodeServicePolicies.OnUpdatePropertiesPolicy;
import org.alfresco.repo.policy.Behaviour;
import org.alfresco.repo.policy.Behaviour.NotificationFrequency;
import org.alfresco.repo.policy.JavaBehaviour;
import org.alfresco.repo.policy.PolicyComponent;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.site.SiteService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.alfresco.util.PropertyMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import fr.jeci.pristy.model.ActeModel;
import fr.jeci.pristy.model.PristyModel;
import fr.jeci.pristy.service.PristyService;

/**
 * @author Jeci - Cindy PIASSALE
 *
 */
public class SpecializeEspacePolicy implements OnAddAspectPolicy, OnUpdatePropertiesPolicy {
	private static Logger logger = Logger.getLogger(SpecializeEspacePolicy.class);

	private PolicyComponent policyComponent;
	private SiteService siteService;
	private NodeService nodeService;
	private NamespaceService namespaceService;
	private PristyService pristyService;

	public void init() {
		// Ajout de l'aspect pm:specializeEspace
		Behaviour onAspectSpecializeEspace = new JavaBehaviour(this, "onAddAspect",
				NotificationFrequency.TRANSACTION_COMMIT);
		this.policyComponent.bindClassBehaviour(OnAddAspectPolicy.QNAME, PristyModel.ASPECT_SPECIALIZE_ESPACE,
				onAspectSpecializeEspace);

		// Mise à jour de la propriété pm:typeEspace
		Behaviour onPropertyTypeEspace = new JavaBehaviour(this, "onUpdateProperties",
				NotificationFrequency.TRANSACTION_COMMIT);
		this.policyComponent.bindClassBehaviour(OnUpdatePropertiesPolicy.QNAME, PristyModel.ASPECT_SPECIALIZE_ESPACE,
				onPropertyTypeEspace);
	}

	@Override
	public void onUpdateProperties(NodeRef nodeRef, Map<QName, Serializable> before, Map<QName, Serializable> after) {
		if (logger.isDebugEnabled()) {
			logger.debug("START SpecializeEspacePolicy - onUpdateProperties");
		}

		// Check le changement de la propriété
		String beforeType = (String) before.get(PristyModel.PROP_TYPE_ESPACE);
		String afterType = (String) after.get(PristyModel.PROP_TYPE_ESPACE);

		if (StringUtils.isNotBlank(afterType) && !afterType.equals(beforeType)) {
			specializeEspace(nodeRef, afterType);
		}

		if (logger.isDebugEnabled()) {
			logger.debug("END SpecializeEspacePolicy - onUpdateProperties");
		}

	}

	@Override
	public void onAddAspect(NodeRef nodeRef, QName aspectTypeQName) {
		if (logger.isDebugEnabled()) {
			logger.debug("START SpecializeEspacePolicy - onAddAspect");
		}

		// Récupération du type d'espace
		String typeEspace = (String) nodeService.getProperty(nodeRef, PristyModel.PROP_TYPE_ESPACE);
		if (StringUtils.isNotBlank(typeEspace)) {
			specializeEspace(nodeRef, typeEspace);
		}

		if (logger.isDebugEnabled()) {
			logger.debug("END SpecializeEspacePolicy - onAddAspect");
		}
	}

	/**
	 * Ajout de specification sur le site en fonction du type d'espace - restriction de permission - création d'un
	 * utilisateur de publication
	 * 
	 * @param site       : <code>NodeRef</code> du site
	 * @param typeEspace : type d'espace
	 */
	private void specializeEspace(NodeRef site, String typeEspace) {
		// Ajout de la restriction sur documentLibrary
		NodeRef docLib = siteService.getContainer(siteService.getSiteShortName(site), SiteService.DOCUMENT_LIBRARY);
		if (StringUtils.isBlank(typeEspace)) {
			// Suppression des restrictions existantes
			this.nodeService.removeAspect(docLib, PristyModel.ASPECT_RESTRICTED_CREATION);
		} else if (PristyModel.ESPACE_TYPE_ACTE.equals(typeEspace)) {
			// Limiter la création au dossier de seance et au dossier standard
			if (logger.isInfoEnabled()) {
				logger.info("Add 'pm:restrictedCreation' for 'cm:folder' and 'am:dossierSeance'");
			}

			List<String> typesAuthorized = new ArrayList<>(1);
			typesAuthorized.add(ContentModel.TYPE_FOLDER.toPrefixString(this.namespaceService));
			typesAuthorized.add(ActeModel.TYPE_DOSSIER_SEANCE.toPrefixString(this.namespaceService));
			Map<QName, Serializable> props = new HashMap<>(1);
			props.put(PristyModel.PROP_AUTHORIZED_TYPES, (Serializable) typesAuthorized);
			this.nodeService.addAspect(docLib, PristyModel.ASPECT_RESTRICTED_CREATION, props);

			// Ajout de l'aspect publication
			if (!this.nodeService.hasAspect(site, PristyModel.ASPECT_INFOS_PUBLICATION)) {
				// Création de l'utilisateur de publication
				String userName = String.format(ActeModel.PATTERN_USER_PUBLICATION, siteService.getSiteShortName(site));
				PropertyMap propsUser = new PropertyMap(3);
				propsUser.put(ContentModel.PROP_USERNAME, userName);
				propsUser.put(ContentModel.PROP_FIRSTNAME, ActeModel.USER_PUBLICATION_FIRSTNAME);
				propsUser.put(ContentModel.PROP_LASTNAME, siteService.getSiteShortName(site));
				String password = pristyService.createUserTechnique(userName, propsUser, ActeModel.ZONE_ACTES);

				// Ajout des infos de l'aspect
				PropertyMap propsPublication = new PropertyMap(3);
				propsPublication.put(PristyModel.PROP_PUB_USERNAME, userName);
				propsPublication.put(PristyModel.PROP_PUB_PASSWORD, password);
				propsPublication.put(PristyModel.PROP_PUB_LINK, this.pristyService.generatePublicationLink());
				this.nodeService.addAspect(site, PristyModel.ASPECT_INFOS_PUBLICATION, propsPublication);
			}
		}
	}

	// -------------------------------------------------------------------------- Setters Methods

	/**
	 * @param policyComponent the policyComponent to set
	 */
	public void setPolicyComponent(PolicyComponent policyComponent) {
		this.policyComponent = policyComponent;
	}

	/**
	 * @param siteService the siteService to set
	 */
	public void setSiteService(SiteService siteService) {
		this.siteService = siteService;
	}

	/**
	 * @param nodeService the nodeService to set
	 */
	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}

	/**
	 * @param namespaceService the namespaceService to set
	 */
	public void setNamespaceService(NamespaceService namespaceService) {
		this.namespaceService = namespaceService;
	}

	/**
	 * @param pristyService the pristyService to set
	 */
	public void setPristyService(PristyService pristyService) {
		this.pristyService = pristyService;
	}

}
