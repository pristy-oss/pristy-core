/**
* Copyright 2021 - Jeci SARL - https://jeci.fr
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public License
* along with this program.  If not, see http://www.gnu.org/licenses/.
*/
package fr.jeci.pristy.service.impl;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.repo.security.authentication.AuthenticationException;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.security.AuthorityService;
import org.alfresco.service.cmr.security.MutableAuthenticationService;
import org.alfresco.service.cmr.security.PersonService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.alfresco.util.collections.CollectionUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;

import fr.jeci.pristy.model.PristyModel;
import fr.jeci.pristy.service.PristyService;

/**
 * @author Jeci - Cindy PIASSALE
 *
 */
public class PristyServiceImpl implements PristyService {

	private static Logger logger = Logger.getLogger(PristyServiceImpl.class);

	private NodeService nodeService;
	private NamespaceService namespaceService;
	private AuthorityService authorityService;
	private PersonService personService;
	private MutableAuthenticationService authenticationService;

	@Override
	public boolean canCreateNode(ChildAssociationRef childAssoc) {
		// Folder parent and document
		NodeRef parent = childAssoc.getParentRef();
		NodeRef node = childAssoc.getChildRef();

		// Test if has aspect pm:restrictedCreation
		if (!this.nodeService.hasAspect(parent, PristyModel.ASPECT_RESTRICTED_CREATION)) {
			return true;
		}

		// Test si le type du node est authorisé
		List<String> typesAuthorized = (List<String>) this.nodeService.getProperty(parent,
				PristyModel.PROP_AUTHORIZED_TYPES);
		if (CollectionUtils.isEmpty(typesAuthorized)) {
			return true;
		}
		for (String typeAuthorized : typesAuthorized) {
			QName type = this.nodeService.getType(node);
			if (typeAuthorized.equals(type.toPrefixString(namespaceService))) {
				return true;
			}
		}

		return false;
	}

	@Override
	public String createUserTechnique(String userName, Map<QName, Serializable> props, String zone) {
		final String password = generateRandomString(PASSWORD_LENGTH, PASSWORD_CHARS);

		AuthenticationUtil.pushAuthentication();
		try {
			AuthenticationUtil.setRunAsUserSystem();
			if (logger.isInfoEnabled()) {
				logger.info(String.format("CreateUser %s dans la zone %s", userName, zone));
			}

			// Check si l'utilisateur existe
			if (!this.personService.personExists(userName)) {

				// Création de la zone
				authorityService.getOrCreateZone(zone);
				Set<String> zones = new HashSet<>(1);
				zones.add(zone);

				// Création du profil utilisateur
				personService.createPerson(props, zones);
			}

			// Création du compte utilisateur
			if (authenticationService.authenticationExists(userName)) {
				authenticationService.setAuthentication(userName, password.toCharArray());
			} else {
				authenticationService.createAuthentication(userName, password.toCharArray());
				authenticationService.setAuthenticationEnabled(userName, true);
			}
		} catch (AuthenticationException e) {
			throw new AlfrescoRuntimeException("Echec de la création du compte utilisateur");
		} finally {
			AuthenticationUtil.popAuthentication();
		}

		return password;
	}

	@Override
	public String generatePublicationLink() {
		return RandomStringUtils.random(LINK_PUBLICATION_LENGTH, LINK_PUBLICATION_CHARS);
	}

	/**
	 * Génération d'une chaîne de caractères aléatoires
	 *
	 * @param length               : nombre de caractères dans la chaine
	 * @param authorizedCharacters : caractères utilisés pour générer la chaine
	 * @return la chaine de caractère aléatoire
	 */
	private String generateRandomString(int length, String authorizedCharacters) {
		return RandomStringUtils.random(length, authorizedCharacters);
	}

	// -------------------------------------------------------------------------- Setters Methods

	/**
	 * @param nodeService the nodeService to set
	 */
	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}

	/**
	 * @param namespaceService the namespaceService to set
	 */
	public void setNamespaceService(NamespaceService namespaceService) {
		this.namespaceService = namespaceService;
	}

	/**
	 * @param authorityService the authorityService to set
	 */
	public void setAuthorityService(AuthorityService authorityService) {
		this.authorityService = authorityService;
	}

	/**
	 * @param authenticationService the authenticationService to set
	 */
	public void setAuthenticationService(MutableAuthenticationService authenticationService) {
		this.authenticationService = authenticationService;
	}

	/**
	 * @param personService the personService to set
	 */
	public void setPersonService(PersonService personService) {
		this.personService = personService;
	}

}
