/**
* Copyright 2021 - Jeci SARL - https://jeci.fr
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public License
* along with this program.  If not, see http://www.gnu.org/licenses/.
*/
package fr.jeci.pristy.service;

import java.io.Serializable;
import java.util.Map;

import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.namespace.QName;

/**
 * @author Jeci - Cindy PIASSALE
 *
 */
public interface PristyService {

	static final int PASSWORD_LENGTH = 12;
	static final String PASSWORD_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	static final int LINK_PUBLICATION_LENGTH = 6;
	static final String LINK_PUBLICATION_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	/**
	 * Test les types authorisés à la création
	 * 
	 * @param childAssoc : association à tester
	 * @return <code>true</code> ou <code>false</code>
	 */
	boolean canCreateNode(ChildAssociationRef childAssoc);

	/**
	 * Création d'un utilisateur technique dans la zone spécifiée
	 * 
	 * @param userName : login de l'utilisateur
	 * @param props    : propriétés du profil utilisateur
	 * @param zone     : zone d'attache du profil
	 * @return le mot de passe généré de l'utilisateur
	 */
	String createUserTechnique(String userName, Map<QName, Serializable> props, String zone);

	/**
	 * Création du lien de publication
	 * 
	 * @return le lien de publication
	 */
	String generatePublicationLink();

}
