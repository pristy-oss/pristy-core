/**
* Copyright 2021 - Jeci SARL - https://jeci.fr
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public License
* along with this program.  If not, see http://www.gnu.org/licenses/.
*/
package fr.jeci.pristy.service;

import org.alfresco.service.cmr.repository.NodeRef;

/**
 * @author Jeci - Cindy PIASSALE
 *
 */
public interface ActeService {

	static final String PATTERN_FOLDER_SEANCE = "Séance du %s";
	static final String FORMAT_DATE_SEANCE = "EEEE d MMMM yyyy";

	/**
	 * Déplacer le dossier de séance dans le dossier année correspondant
	 * 
	 * @param folderSeance : <code>NodeRef</code> du dossier de séance
	 */
	void moveFolderSeance(NodeRef folderSeance);

	/**
	 * Test si on est dans un espace de travail de type Acte
	 * 
	 * @param node : <code>NodeRef</code> du noeud à tester
	 * @return <code>true</code> ou <code>false</code>
	 */
	boolean isInWorkspaceActe(NodeRef node);

}
