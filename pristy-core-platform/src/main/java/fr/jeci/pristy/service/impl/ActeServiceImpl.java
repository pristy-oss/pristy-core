package fr.jeci.pristy.service.impl;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.site.SiteInfo;
import org.alfresco.service.cmr.site.SiteService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;

import fr.jeci.pristy.model.ActeModel;
import fr.jeci.pristy.model.PristyModel;
import fr.jeci.pristy.service.ActeService;

public class ActeServiceImpl implements ActeService {

	private static Logger logger = Logger.getLogger(ActeServiceImpl.class);

	private NodeService nodeService;
	private SiteService siteService;
	private FileFolderService fileFolderService;
	private NamespaceService namespaceService;

	@Override
	public void moveFolderSeance(NodeRef folderSeance) {
		if (logger.isDebugEnabled()) {
			logger.debug("ActeService - moveFolderSeance - nodeRef : " + folderSeance);
		}

		// Récupération de la date
		Date dateSeance = (Date) this.nodeService.getProperty(folderSeance, ActeModel.PROP_DATE_SEANCE);

		// Create folder 'annee' yyyy
		SiteInfo site = this.siteService.getSite(folderSeance);
		NodeRef docLib = this.siteService.getContainer(site.getShortName(), SiteService.DOCUMENT_LIBRARY);
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateSeance);
		int year = cal.get(Calendar.YEAR);
		NodeRef yearFolder = getOrCreateYearFolder(docLib, Integer.toString(year));

		// Rename le dossier seance - Séance du EEEE d MMMM yyyy
		SimpleDateFormat format = new SimpleDateFormat(FORMAT_DATE_SEANCE, Locale.FRENCH);
		String folderName = String.format(PATTERN_FOLDER_SEANCE, format.format(cal.getTime()));

		// Move folder 'seance' into folder 'annee'
		QName assocQName = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, folderName);
		this.nodeService.moveNode(folderSeance, yearFolder, ContentModel.ASSOC_CONTAINS, assocQName);
		this.nodeService.setProperty(folderSeance, ContentModel.PROP_NAME, folderName);

		// Restriction de création au document de type 'acte'
		List<String> typesAuthorized = new ArrayList<>(1);
		typesAuthorized.add(ActeModel.TYPE_DOCUMENT_ACTE.toPrefixString(this.namespaceService));
		Map<QName, Serializable> props = new HashMap<>(1);
		props.put(PristyModel.PROP_AUTHORIZED_TYPES, (Serializable)typesAuthorized);
		this.nodeService.addAspect(folderSeance, PristyModel.ASPECT_RESTRICTED_CREATION, props);
	}

	@Override
	public boolean isInWorkspaceActe(NodeRef node) {
		// Récupération du noeud st:site
		SiteInfo site = this.siteService.getSite(node);

		// Test si le site est de type acte
		return site != null && PristyModel.ESPACE_TYPE_ACTE
				.equals(this.nodeService.getProperty(site.getNodeRef(), PristyModel.PROP_TYPE_ESPACE));
	}

	/**
	 * Get or create le folder annee
	 * 
	 * @param docLib : <code>NodeRef</code> du container documentLibrary
	 * @param year   : année yyyy
	 * @return <code>NodeRef</code> du dossier
	 */
	private NodeRef getOrCreateYearFolder(NodeRef docLib, String year) {
		NodeRef yearFolder = this.fileFolderService.searchSimple(docLib, year);
		if (yearFolder == null) {
			Map<QName, Serializable> props = new HashMap<>(1);
			props.put(ContentModel.PROP_NAME, year);
			QName assocQName = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, year);
			yearFolder = this.nodeService
					.createNode(docLib, ContentModel.ASSOC_CONTAINS, assocQName, ContentModel.TYPE_FOLDER, props)
					.getChildRef();
		}

		// Restriction de création au dossier de type am:dossierSeance
		List<String> typesAuthorized = new ArrayList<>(1);
		typesAuthorized.add(ActeModel.TYPE_DOSSIER_SEANCE.toPrefixString(this.namespaceService));
		Map<QName, Serializable> props = new HashMap<>(1);
		props.put(PristyModel.PROP_AUTHORIZED_TYPES, (Serializable)typesAuthorized);
		this.nodeService.addAspect(yearFolder, PristyModel.ASPECT_RESTRICTED_CREATION, props);
		return yearFolder;
	}

	// -------------------------------------------------------------------------- Setters Methods

	/**
	 * @param nodeService the nodeService to set
	 */
	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}

	/**
	 * @param siteService the siteService to set
	 */
	public void setSiteService(SiteService siteService) {
		this.siteService = siteService;
	}

	/**
	 * @param fileFolderService the fileFolderService to set
	 */
	public void setFileFolderService(FileFolderService fileFolderService) {
		this.fileFolderService = fileFolderService;
	}

	/**
	 * @param namespaceService the namespaceService to set
	 */
	public void setNamespaceService(NamespaceService namespaceService) {
		this.namespaceService = namespaceService;
	}

}
