/**
* Copyright 2021 - Jeci SARL - https://jeci.fr
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public License
* along with this program.  If not, see http://www.gnu.org/licenses/.
*/
package fr.jeci.pristy.model;

import org.alfresco.service.namespace.QName;

/**
 * @author Jeci - Cindy PIASSALE
 *
 */
public interface ActeModel {

	static final String NAMESPACE_URI = "https://jeci.fr/model/pristy/delib/1.0";
	static final String NAMESPACE_PREFIX = "am";

	// Type am:dossierSeance
	static final QName TYPE_DOSSIER_SEANCE = QName.createQName(NAMESPACE_URI, "dossierSeance");

	// Type am:documentActe
	static final QName TYPE_DOCUMENT_ACTE = QName.createQName(NAMESPACE_URI, "documentActe");
	static final QName PROP_TYPE_ACTE = QName.createQName(NAMESPACE_URI, "typeActe");
	static final QName PROP_CODE_ACTE = QName.createQName(NAMESPACE_URI, "codeActe");
	static final QName ASSOC_ANNEXES_CONSTITUTIVES = QName.createQName(NAMESPACE_URI, "annexesConstitutives");

	// Type am:annexeConstitutive
	static final QName TYPE_ANNEXE_CONSTITUTIVE = QName.createQName(NAMESPACE_URI, "annexeConstitutive");

	// Aspect am:dateSeance
	static final QName ASPECT_DATE_SEANCE = QName.createQName(NAMESPACE_URI, "dateSeance");
	static final QName PROP_DATE_SEANCE = QName.createQName(NAMESPACE_URI, "dateSeance");

	// Liste des types acte
	static final String TYPE_ACTE_DE = "DE";
	static final String TYPE_ACTE_AR = "AR";
	static final String TYPE_ACTE_AI = "AI";
	static final String TYPE_ACTE_CC = "CC";
	static final String TYPE_ACTE_BF = "BF";
	static final String TYPE_ACTE_AU = "AU";

	// Utilisateurs techniques pour les sites actes
	static final String ZONE_ACTES = "APP.ACTES";
	static final String PATTERN_USER_PUBLICATION = "user_publication_%s";
	static final String USER_PUBLICATION_FIRSTNAME = "Publication";
}
