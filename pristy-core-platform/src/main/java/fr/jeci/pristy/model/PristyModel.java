/**
* Copyright 2021 - Jeci SARL - https://jeci.fr
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public License
* along with this program.  If not, see http://www.gnu.org/licenses/.
*/
package fr.jeci.pristy.model;

import org.alfresco.service.namespace.QName;

/**
 * @author Jeci - Cindy PIASSALE
 *
 */
public interface PristyModel {

	static final String NAMESPACE_URI = "https://jeci.fr/model/pristy/core/1.0";
	static final String NAMESPACE_PREFIX = "pm";

	// Aspect pm:noChildren
	static final QName ASPECT_NO_CHILDREN = QName.createQName(NAMESPACE_URI, "noChildren");

	// Aspect pm:restrictedCreation
	static final QName ASPECT_RESTRICTED_CREATION = QName.createQName(NAMESPACE_URI, "restrictedCreation");
	static final QName PROP_AUTHORIZED_TYPES = QName.createQName(NAMESPACE_URI, "authorizedTypes");

	// Aspect pm:specializeEspace
	static final QName ASPECT_SPECIALIZE_ESPACE = QName.createQName(NAMESPACE_URI, "specializeEspace");
	static final QName PROP_TYPE_ESPACE = QName.createQName(NAMESPACE_URI, "typeEspace");

	// Aspect pm:infosPublication
	static final QName ASPECT_INFOS_PUBLICATION = QName.createQName(NAMESPACE_URI, "infosPublication");
	static final QName PROP_PUB_USERNAME = QName.createQName(NAMESPACE_URI, "pubUserName");
	static final QName PROP_PUB_PASSWORD = QName.createQName(NAMESPACE_URI, "pubPassword");
	static final QName PROP_PUB_LINK = QName.createQName(NAMESPACE_URI, "pubLink");

	// Type d'espace
	static final String ESPACE_TYPE_ACTE = "actes";
}
