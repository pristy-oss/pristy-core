/**
* Copyright 2021 - Jeci SARL - https://jeci.fr
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public License
* along with this program.  If not, see http://www.gnu.org/licenses/.
*/
package fr.jeci.pristy.webscripts.authentication;

import java.util.HashMap;
import java.util.Map;

import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.security.AuthenticationService;
import org.alfresco.service.cmr.site.SiteInfo;
import org.alfresco.service.cmr.site.SiteService;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.extensions.webscripts.Cache;
import org.springframework.extensions.webscripts.DeclarativeWebScript;
import org.springframework.extensions.webscripts.Status;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;

import fr.jeci.pristy.model.PristyModel;

/**
 * @author Jeci - Cindy PIASSALE
 *
 */
public class GetTicketPublication extends DeclarativeWebScript {

	private static Log logger = LogFactory.getLog(GetTicketPublication.class);

	private static final String TEMPLATE_ARGS_TYPE = "type";
	private static final String TEMPLATE_ARGS_ID = "id";
	private static final String TYPE_SITE = "site";
	private static final String TYPE_NODE = "node";

	private NodeService nodeService;
	private SiteService siteService;
	private AuthenticationService authenticationService;

	@Override
	protected Map<String, Object> executeImpl(WebScriptRequest req, Status status, Cache cache) {
		if (logger.isDebugEnabled()) {
			logger.debug("GetTicketPublication START");
		}
		Map<String, Object> model = new HashMap<>(2);

		AuthenticationUtil.pushAuthentication();
		try {
			AuthenticationUtil.setRunAsUserSystem();
			// Get template args
			String type = req.getServiceMatch().getTemplateVars().get(TEMPLATE_ARGS_TYPE);
			String id = req.getServiceMatch().getTemplateVars().get(TEMPLATE_ARGS_ID);
			NodeRef nodeRef = null;

			// Recherche d'un site
			if (TYPE_SITE.equals(type)) {
				SiteInfo site = this.siteService.getSite(id);
				if (site != null) {
					nodeRef = site.getNodeRef();
				}
			}

			// Recherche d'un noeud
			if (TYPE_NODE.equals(type)) {
				nodeRef = new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, id);
			}

			if (nodeRef == null) {
				throw new WebScriptException(HttpStatus.SC_BAD_REQUEST,
						"Aucun noeud n'a été trouvé à partir de la référence " + id);
			}

			if (!this.nodeService.hasAspect(nodeRef, PristyModel.ASPECT_INFOS_PUBLICATION)) {
				throw new WebScriptException(HttpStatus.SC_INTERNAL_SERVER_ERROR, "Pas d'informations de publication");
			}

			String userName = (String) this.nodeService.getProperty(nodeRef, PristyModel.PROP_PUB_USERNAME);
			String password = (String) this.nodeService.getProperty(nodeRef, PristyModel.PROP_PUB_PASSWORD);
			
			if (!this.authenticationService.authenticationExists(userName)) {
				throw new WebScriptException(HttpStatus.SC_INTERNAL_SERVER_ERROR, "Authentificaiton impossible pour l'utilisateur " + userName);
			}

			authenticationService.authenticate(userName, password.toCharArray());

			model.put("ticket", authenticationService.getCurrentTicket());
			model.put("login", authenticationService.getCurrentUserName());
		} finally {
			AuthenticationUtil.popAuthentication();
		}

		if (logger.isDebugEnabled()) {
			logger.debug("GetTicketPublication END");
		}
		return model;
	}

	// -------------------------------------------------------------------------- Setters Methods

	/**
	 * @param nodeService the nodeService to set
	 */
	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}

	/**
	 * @param siteService the siteService to set
	 */
	public void setSiteService(SiteService siteService) {
		this.siteService = siteService;
	}

	/**
	 * @param authenticationService the authenticationService to set
	 */
	public void setAuthenticationService(AuthenticationService authenticationService) {
		this.authenticationService = authenticationService;
	}

}
